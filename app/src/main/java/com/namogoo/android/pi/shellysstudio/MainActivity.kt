package com.namogoo.android.pi.shellysstudio

import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            this.supportActionBar!!.hide()
        } catch (e: NullPointerException) {
        }
        setContentView(R.layout.activity_main)

        wv.settings.javaScriptEnabled = true
        wv.settings.setLoadWithOverviewMode(true)
        wv.settings.setUseWideViewPort(true)
        wv.settings.setBuiltInZoomControls(false)
        wv.settings.setAppCacheEnabled(false)
        wv.settings.setCacheMode(WebSettings.LOAD_NO_CACHE)
        wv.settings.setJavaScriptCanOpenWindowsAutomatically(true)
        wv.settings.setDomStorageEnabled(true)
        wv.setFocusableInTouchMode(false)
        wv.setFocusable(false)
        wv.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                closeSplash()
            }
        }
        wv.loadUrl("https://shelly-studio.com/")
        var handler : Handler = Handler(this.mainLooper)
        handler.postDelayed({
            closeSplash()
        }, 10000)
    }

    fun closeSplash () {
        var handler : Handler = Handler(this.mainLooper)
        wv.visibility = View.VISIBLE
        handler.postDelayed({
            splash.visibility = View.GONE
        }, 100)

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (wv.canGoBack()) {
                        wv.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}